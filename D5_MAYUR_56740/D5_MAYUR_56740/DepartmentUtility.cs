﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D5_MAYUR_56740
{
    public static class DepartmentUtility
    {
        //write a function to add department  into collections
        public static List<Department> Add_Department(List<Department> departmentList)
        {
            Department department = new Department();
            Console.WriteLine("Enter Department number : ");
            department.DeptNo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Department name : ");
            department.DeptName = Console.ReadLine();
            Console.WriteLine("Enter Department location : ");
            department.Location = Console.ReadLine();

            departmentList.Add(department);
            Console.WriteLine("Department record added successfully...");

            return departmentList;
        }

        //To Display all department deatils..
        public static void Display_All_Departments(List<Department> departmentList)
        {
            foreach (var department in departmentList)
            {
                Console.WriteLine(department);
            }
        }
    }
}
