﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D5_MAYUR_56740
{
    public class Employee : IComparable<Employee>
    {
        //Employee(EmpNo, Name, Designaion, Salary, Commision,DeptNo) 
        private int _EmpNo;
        private string _Name;
        private string _Designation;
        private int _Salary;
        private int _Commission;
        private int _DeptNo;

        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }


        public int Commission
        {
            get { return _Commission; }
            set { _Commission = value; }
        }


        public int Salary
        {
            get { return _Salary; }
            set { _Salary = value; }
        }


        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }


        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int EmpNo
        {
            get { return _EmpNo; }
            set { _EmpNo = value; }
        }

        public int CompareTo(Employee other)
        {
            if (other._Salary > this._Salary)
                return -1;
            else if (other._Salary == this._Salary)
                return 0;
            else
                return 1;
        }

        public override string ToString()
        {
            return string.Format("# Employee Details :: Number-{0} Name-{1} Designation-{2} Salary-{3} Commission-{4} Department Number-{5}.", this._EmpNo, this._Name, this._Designation, this._Salary, this._Commission, this._DeptNo);
        }

    }
}
