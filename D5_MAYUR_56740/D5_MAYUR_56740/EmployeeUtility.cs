﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D5_MAYUR_56740
{
     public static class EmployeeUtility
    {
        //write a function to add employee  into collections
        public static List<Employee> Add_Employee(List<Employee> employeeList)
        {
            Console.WriteLine("\nData Entry for Employee :::");

            Employee employee = new Employee();
            Console.WriteLine("Enter employee number : ");
            employee.EmpNo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter employee name : ");
            employee.Name = Console.ReadLine();
            Console.WriteLine("Enter employee designation : ");
            employee.Designation = Console.ReadLine();
            Console.WriteLine("Enter employee salary : ");
            employee.Salary = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter employee commission : ");
            employee.Commission = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter employee department number : ");
            employee.DeptNo = Convert.ToInt32(Console.ReadLine());

            employeeList.Add(employee);
            Console.WriteLine("Employee record added successfully...");

            return employeeList;
        }

        //To Display all employee deatils..
        public static void Display_All_Employees(List<Employee> employeeList)
        {
            foreach (var employee in employeeList)
            {
                Console.WriteLine(employee);
            }
        }

        //To Display total salary ofall employees..
        public static double Display_Total_Salary_Of_All_Employees(List<Employee> employeeList)
        {
            double totalSalary = 0;
            Console.WriteLine("\n***************************************************************************************\n");
            foreach (var employee in employeeList)
            {
                Console.WriteLine("##  Emplyoee-Name::{0}  Salary-{1}  Commission-{2}  Total-Salary-{3}.", employee.Name, employee.Salary, employee.Commission, (employee.Salary + employee.Commission));
                totalSalary = totalSalary + (employee.Salary + employee.Commission);
            }
            Console.WriteLine("\n***************************************************************************************\n");
            return totalSalary;
        }

        public static void GetAllEmployeesByDept(int deptNo, List<Employee> employeeList)
        {
            var result = (from emp in employeeList
                          where emp.DeptNo == deptNo
                          select emp);
            Console.WriteLine("\n***************************************************************************************\n");
            Console.WriteLine("## List of all employees from department number {0}\n", deptNo);
            foreach (var empl in result)
            {
                Console.WriteLine("## "+ empl);
            }
            Console.WriteLine("\n***************************************************************************************\n");
        }

        public static int GetAllEmployeesCountByDept(int deptNo, List<Employee> employeeList)
        {
            var result = (from emp in employeeList
                          where emp.DeptNo == deptNo
                          select emp);
            int count = result.Count();
            return count;
        }

        public static double GetAverageSalaryByDept(int deptNo, List<Employee> employeeList)
        {
            var result = (from emp in employeeList
                          where emp.DeptNo == deptNo
                          select emp);
            double averageSalary = 0;
            int totalSum = 0;
            foreach (Employee emp in result)
            {
                totalSum = totalSum + emp.Salary;
            }
            averageSalary = totalSum / (result.Count());
            return averageSalary;
        }

        public static Employee GetMinSalaryByDept(int deptNo, List<Employee> employeeList)
        {
            var result = (from emp in employeeList
                          where emp.DeptNo == deptNo
                          select emp);
            Employee emp1 = result.Min();
            return emp1;

        }
    }
}
