﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D5_MAYUR_56740
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***************************************************************************************");
            Console.WriteLine("\n###   Welcome to Employee management App   ###\n");
            Console.WriteLine("***************************************************************************************");

            // Employee List Created
            List<Employee> employeeList = new List<Employee>()
            {

                 new Employee(){ EmpNo=1, Name="Mayur", Designation="HOD", Salary=90000, Commission=10000, DeptNo=1},
                 new Employee(){ EmpNo=2, Name="Pramod", Designation="Assistant Professor", Salary=50000, Commission=5000, DeptNo=1},
                 new Employee(){ EmpNo=3, Name="Vijay", Designation="Assistant Professor", Salary=70000, Commission=5000, DeptNo=1},
                 new Employee(){ EmpNo=4, Name="Hrushi", Designation="Assistant Professor", Salary=40000, Commission=5000, DeptNo=1},
                 new Employee(){ EmpNo=5, Name="Komal", Designation="Assistant Professor", Salary=70000, Commission=5000, DeptNo=2},
                 new Employee(){ EmpNo=6, Name="Nikita", Designation="Assistant Professor", Salary=20000, Commission=5000, DeptNo=2}
            };
            // Department list created
            List<Department> departmentList = new List<Department>()
            {
                new Department(){DeptNo=1,DeptName="Mechanical",Location="Ahmednager"},
                new Department(){DeptNo=2,DeptName="IT",Location="Aurangabad"}

            };


            while (true)
            {
                Console.Write("\nEnter choice for operation from following list ::\n  " +
                "1. To Add employee and department data into collections..\n  " +
                "2. To Display employee and department data from collections..\n  " +
                "3. To Calculate total salary(sal + comm) of all employees..\n  " +
                "4. To Display all employees of particular department..\n  " +
                "5. To Calculate department wise count of employees..\n  " +
                "6. To Calculate department wise average salary..\n  " +
                "7. To Calculate department wise minimum salary..\n## Choice To Operation :: ");

                int choiceToOperation = Convert.ToInt32(Console.ReadLine());
                switch (choiceToOperation)
                {
                    case 1:
                        {
                            while (true)
                            {
                                Console.Write("\n Enter \n  1: To add employee deatils..\n  2: To add department details..\n## Choice To Add Details :: ");
                                int choiceToAddDetails = Convert.ToInt32(Console.ReadLine());

                                if (choiceToAddDetails == 1)
                                {
                                    employeeList = EmployeeUtility.Add_Employee(employeeList);
                                }
                                else if (choiceToAddDetails == 2)
                                {
                                    departmentList = DepartmentUtility.Add_Department(departmentList);
                                }
                                else
                                {
                                    Console.WriteLine("Invalid Choice");
                                }

                                Console.WriteLine("## Do you want to continue ? yes/no ::");
                                string choiceToContinue = Console.ReadLine();
                                if (choiceToContinue == "no")
                                {
                                    break;
                                }
                            }
                            break;
                        }
                    case 2:
                        {
                            Console.Write("\n# Enter \n  1: To Display all employee deatils..\n  2: To Display all department details..\n## Choice To Display Details :: ");
                            int choiceToAddDetails = Convert.ToInt32(Console.ReadLine());

                            if (choiceToAddDetails == 1)
                            {
                                Console.WriteLine("\n***************************************************************************************\n");
                                Console.WriteLine("\n## All Employee details are ::\n");
                                EmployeeUtility.Display_All_Employees(employeeList);
                                Console.WriteLine("\n***************************************************************************************\n");

                            }
                            else if (choiceToAddDetails == 2)
                            {
                                Console.WriteLine("\n***************************************************************************************\n");
                                Console.WriteLine("\n## All Department details are ::\n");
                                DepartmentUtility.Display_All_Departments(departmentList);
                                Console.WriteLine("\n***************************************************************************************\n");
                            }
                            else
                                Console.WriteLine("Invalid choice..");
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("\n## Total salary ( salary + Commission) of all employees is ::");
                            double totalSalary = EmployeeUtility.Display_Total_Salary_Of_All_Employees(employeeList);
                            Console.WriteLine("\n***************************************************************************************\n");
                            Console.WriteLine("\n## Total salary of all employees = {0}\n", totalSalary);
                            Console.WriteLine("\n***************************************************************************************\n");
                            break;
                        }
                    case 4:
                        {
                            Console.Write("\n## Enter department number ::");
                            int deptNo = Convert.ToInt32(Console.ReadLine());
                            EmployeeUtility.GetAllEmployeesByDept(deptNo, employeeList);
                            break;
                        }
                    case 5:
                        {
                            Console.Write("\n## Enter department number ::");
                            int deptNo = Convert.ToInt32(Console.ReadLine());
                            int count = EmployeeUtility.GetAllEmployeesCountByDept(deptNo, employeeList);
                            Console.WriteLine("\n***************************************************************************************\n");
                            Console.WriteLine("## Count of all employees from department number {0} == {1}", deptNo, count);
                            Console.WriteLine("\n***************************************************************************************\n");
                            break;
                        }

                    case 6:
                        {
                            Console.Write("\n## Enter department number ::");
                            int deptNo = Convert.ToInt32(Console.ReadLine());
                            double averageSalary = EmployeeUtility.GetAverageSalaryByDept(deptNo, employeeList);
                            Console.WriteLine("\n***************************************************************************************\n");
                            Console.WriteLine("## Avearge salary of all employees from department number {0} == {1}", deptNo, averageSalary);
                            Console.WriteLine("\n***************************************************************************************\n");
                            break;
                        }

                    case 7:
                        {
                            Console.Write("\n## Enter department number ::");
                            int deptNo = Convert.ToInt32(Console.ReadLine());
                            Employee emp = EmployeeUtility.GetMinSalaryByDept(deptNo, employeeList);
                            Console.WriteLine("\n***************************************************************************************\n");
                            Console.WriteLine("  ## Minimum salary  from department number {0} of employee {2} == {1}", deptNo, emp.Salary, emp.Name);
                            Console.WriteLine("\n***************************************************************************************\n");
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice...");
                            break;
                        }
                }
            }

        }
    }
}
