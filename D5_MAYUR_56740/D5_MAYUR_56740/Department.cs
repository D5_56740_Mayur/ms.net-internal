﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D5_MAYUR_56740
{
    public class Department
    {
        // Department(DeptNo, DeptName, Location) 
        private int _DeptNo;
        private string _DeptName;
        private string _Location;

        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }


        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }


        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }

        public override string ToString()
        {
            return string.Format("# Department number-{0} Name-{1} Location-{2}.", this._DeptNo, this._DeptName, this._Location);
        }

    }
}
